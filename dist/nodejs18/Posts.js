const axios  = require('axios');

let config = {
  method: 'post',
  maxBodyLength: Infinity,
  url: 'https://jsonplaceholder.typicode.com/posts',
  headers: { }
};

exports.getPosts = async () =>{
        return axios.request(config);
}
exports.ReadPosts = () =>{
return "hello from read posts";
}
exports.WritePosts = () =>{
  return "hello from write posts";
}
exports.JumpPosts = () =>{
    return "hello from jump posts";
}