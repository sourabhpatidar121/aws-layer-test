# Remove the "dist" directory if it exists
[ -d "dist" ] && rm -r dist

# Create the required directory structure
mkdir -p dist/nodejs18/

# Copy all files from "src" directory to "dist/nodejs18"
cp -r src/* dist/nodejs18

# Copy package.json to "dist/nodejs18"
cp package.json dist/nodejs18/

#cp -r node_modules dist/nodejs18/


cd dist/nodejs18/

npm install

cd ../../

# Zip the "dist/nodejs18" directory to create the Lambda Layer package hello a litle change
zip -r dist/layer.zip dist/nodejs18

aws lambda publish-layer-version --layer-name test-layer  \
    --description "My layer" \
    --license-info "MIT" \
    --zip-file fileb://dist/layer.zip \
    --compatible-runtimes nodejs18.x 
